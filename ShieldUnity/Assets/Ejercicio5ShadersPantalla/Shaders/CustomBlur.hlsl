#ifndef CUSTOM_BLUR_INCLUDED
#define CUSTOM_BLUR_INCLUDED

void ApplyGaussianBlur3x3_float(UnityTexture2D Tex, UnitySamplerState Ss, float2 Uv, float2 texelSize, float texelOffset, out float4 Result)
{
    float gaussianKernel[9] =
    {0.0625, 0.125, 0.0625, 0.125, 0.25, 0.125, 0.0625, 0.125, 0.0625};
    Result = 0;
    [unroll(9)]
    for (int y = 1; y >= -1; y--)
    {
        for (int x = -1; x < 2; x++)
        {   
            Result += SAMPLE_TEXTURE2D(Tex, Ss, Uv + texelSize * float2(x, y) * texelOffset) * 0.1111112;
        }

    }

}
#endif //#ifndef CUSTOM_BLUR_INCLUDED
