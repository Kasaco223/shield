## Textura de mascara multicanal

![Imagen Multicanal](Multimedia/Multicanal_Imagen.jpg)

![Video Multicanal](Multimedia/Multicanal_Video.mp4)


## Máscara radial procedural

![Imagen Multicanal](Multimedia/Radial_Imagen.jpg)

![Video Radial](Multimedia/Radial_Video.mp4)


## Refracción con normal map

![Imagen Multicanal](Multimedia/Refraction_Imagen.jpeg)

![Video Refracción](Multimedia/Refraction_Video.mp4)


## Movimiento de humo

![Imagen Multicanal](Multimedia/Humo_Imagen.jpeg)

![Video Humo](Multimedia/Humo_Video.mp4)


## Disolver con ruido

![Imagen Multicanal](Multimedia/Noise_Imagen.jpeg)

![Video Noise](Multimedia/Noise_Video.mp4)


## Disolver Textura Pre Creada

![Imagen Multicanal](Multimedia/TPrecreada_Imagen.jpeg)

![Video Tprecreada](Multimedia/TPrecreada_Video.mp4)


## Coordenadas polares

![Imagen Multicanal](Multimedia/Polar_Imagen.jpeg)

![Video Coordenadas Polares](Multimedia/Polar_Video.mp4)


## Textura en espejo

![Imagen Multicanal](Multimedia/Mirror_Imagen.jpeg)

![Video Textura Espejo](Multimedia/Mirror_Video.mp4)