using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmandoController : MonoBehaviour
{
    private Animator animator;
    private bool isAttacking;
    private bool isBlocking;
    private bool isHealing;
    private bool isPunching;

    void Start()
    {
        // Obtener el componente Animator
        animator = GetComponent<Animator>();
        isAttacking = false;
        isHealing = false;
        isPunching = false;
    }

    void Update()
    {
        // Detectar si el bot�n 1 es presionado para ataque
        if (Input.GetKeyDown(KeyCode.Alpha1) && !isAttacking)
        {
            StartCoroutine(StartAnimation("Atack", () => isAttacking = false));
            isAttacking = true;
        }

        // Detectar si el bot�n 2 es presionado para bloqueo
        if (Input.GetKeyDown(KeyCode.Alpha2) && !isPunching)
        {
            StartCoroutine(StartAnimation("Punch", () => isPunching = false));
            isPunching = true;
        }

        // Detectar si el bot�n 3 es presionado para lanzamiento
        if (Input.GetKeyDown(KeyCode.Alpha3) && !isHealing)
        {
            StartCoroutine(StartAnimation("Heal", () => isHealing = false));
            isHealing = true;
        }
    }
    public void Anim1()
    {
        StartCoroutine(StartAnimation("Atack", () => isAttacking = false));
        isAttacking = true;
    }    public void Anim2()
    {
        StartCoroutine(StartAnimation("Punch", () => isPunching = false));
        isPunching = true;
    }    public void Anim3()
    {
        StartCoroutine(StartAnimation("Heal", () => isHealing = false));
        isHealing = true;
    }

    private IEnumerator StartAnimation(string parameterName, System.Action resetAction)
    {
        // Establecer el par�metro en true
        animator.SetBool(parameterName, true);

        // Obtener el tiempo de duraci�n de la animaci�n actual
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        float animationDuration = stateInfo.length;

        // Esperar hasta que la animaci�n termine
        yield return new WaitForSeconds(animationDuration/6f);

        // Establecer el par�metro en false
        animator.SetBool(parameterName, false);

        // Ejecutar la acci�n para resetear el estado
        resetAction();
    }
}
