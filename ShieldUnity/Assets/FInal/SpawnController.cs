using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    // Declarar un objeto p�blico
    public GameObject objectToActivate;
    public GameObject objectToActivate1;
    public GameObject objectToActivate2;

    // M�todo Start es llamado antes de la primera actualizaci�n del frame
    void Start()
    {
        DesactiveAll();
    }

    // M�todo Update es llamado una vez por frame
    void Update()
    {

    }

    // M�todo p�blico para activar el objeto
    public void ActivateObject1()
    {
        if (objectToActivate != null)
        {
            objectToActivate.SetActive(true);
            objectToActivate1.SetActive(false);
            objectToActivate2.SetActive(false);
            Invoke("DesactiveAll", 2f);
        }
    }
    public void ActivateObject2()
    {
        if (objectToActivate1 != null)
        {
            objectToActivate1.SetActive(true);
            objectToActivate.SetActive(false);
            objectToActivate2.SetActive(false);
            Invoke("DesactiveAll", 2f);
        }
    }
    public void ActivateObject3()
    {
        if (objectToActivate2 != null)
        {
            objectToActivate2.SetActive(true);
            objectToActivate1.SetActive(false);
            objectToActivate.SetActive(false);
            Invoke("DesactiveAll", 2f);
        }
    }
    private void DesactiveAll()
    {
        objectToActivate2.SetActive(false);
        objectToActivate1.SetActive(false);
        objectToActivate.SetActive(false);
    }

}
