using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    Camera _cam;

    private void Start()
    {
        _cam = Camera.main;
    }

    private void Update()
    {
        Vector3 lookDirection = _cam.transform.position - transform.position;
        transform.rotation = Quaternion.LookRotation(lookDirection);
    }
}
