Shader "Custom/LookAtCameraShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                // Get direction vector from fragment to camera
                float3 direction = _WorldSpaceCameraPos - i.vertex.xyz;

                // Normalize the direction vector
                direction = normalize(direction);

                // Convert direction to UV coordinates
                float2 uv = float2(atan2(direction.z, direction.x) / (2 * 3.14159) + 0.5, asin(direction.y) / 3.14159 + 0.5);

                // Sample the scene color texture using the calculated UV coordinates
                float4 sceneColor = tex2D(_MainTex, uv);

                return sceneColor;
            }
            ENDCG
        }
    }
}
