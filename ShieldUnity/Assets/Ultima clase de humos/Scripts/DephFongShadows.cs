using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

[Serializable]
public class DephFongShadows : VolumeComponent, IPostProcessComponent 
{
    public ClampedFloatParameter weight = new ClampedFloatParameter(value: 0, min: 0,max: 1);
    public FloatParameter nearDistance = new FloatParameter(0);
    public FloatParameter farDistance = new FloatParameter(10);
    public ColorParameter fogColor = new ColorParameter(new Color(r:0.29f,g:0.69f,b:0f));

    public bool IsActive() => true;
    public bool IsTileCompatible() => true;

}
